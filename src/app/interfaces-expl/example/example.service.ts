import { Injectable } from "@angular/core";
import { Animal } from "./models/classes/animal.class";
import { Bird } from "./models/classes/bird.class";
import { Fish } from "./models/classes/fish.class";
import { LivingCreature } from "./models/classes/living-thing.class";
import { Person } from "./models/classes/person.class";
import { FlapYOurGills } from "./models/interfaces/flap-your-gills";

@Injectable({
  providedIn: "root",
})
export class ExampleService {
  constructor() {}

  executeInitialFunctions() {
    let creature1: LivingCreature = new LivingCreature(1, "Amoeba");
    // console.log(LivingCreature);
    // creature1.name = "Amoeba";
    creature1.numberOfHearts = 0;

    // console.log(creature1);
    // creature1.breathe();

    // Animal from here

    let dog: LivingCreature;
    /*...
    ..sdfkh
    sfjdsf
    .*/
    dog = new Animal("DOG", 1, 4);
    // console.log(dog);
    // dog.breathe();

    //Person starts from here
    let sreekesh = new Person("Sreekesh");
    // sreekesh.think();

    let akshay: Animal = new Person("Akshay");
    // akshay.think(); --> We cannot do this
    // (akshay as Person).think();

    // dog.move();
    // akshay.move();

    let kaki = new Bird("Kaki", 1, 2);
    // kaki.move();

    let soraChapa = new Fish(1, "Sora Chapa");
    // soraChapa.move();

    // console.log(soraChapa.wannaFuck(dog.name));
    // console.log(soraChapa.wannaFuck(sreekesh.name));

    // let tryinglsdkfj = new FlapYOurGills(); -- wrong.. interfaces are just rules

    let crazyCreature: FlapYOurGills;

    crazyCreature = new Fish(1, "Sollu Chaapa");
    // crazyCreature.wannaFuck("Sreeksh");

    let arrayOfGuys = [];
    arrayOfGuys.push(dog);
    arrayOfGuys.push(soraChapa);
    arrayOfGuys.push(sreekesh);
    arrayOfGuys.push(akshay);
    this.decideWhetherWannaFuckThisGuys(crazyCreature, arrayOfGuys);
  }

  decideWhetherWannaFuckThisGuys(
    flapYourGilssCreature: FlapYOurGills,
    guysNames: Animal[]
  ): void {
    for (let i = 0; i < guysNames.length; i++) {
      let currentGuy = guysNames[i];
      if (flapYourGilssCreature.wannaFuck(currentGuy.name)) {
        console.log("yes I wanna Fuck " + currentGuy.name);
      } else {
        console.log("yes I dont wanna Fuck " + currentGuy.name);
      }
    }
  }
}
