import { Component, OnInit } from '@angular/core';
import { ExampleService } from './example.service';

@Component({
  selector: 'inheritence-polymorphism',
  templateUrl: './example.component.html',
  styleUrls: ['./example.component.scss']
})
export class ExampleComponent implements OnInit {

  constructor(private interfaceExmple: ExampleService) { }

  ngOnInit(): void {
    this.interfaceExmple.executeInitialFunctions();
  }

}
