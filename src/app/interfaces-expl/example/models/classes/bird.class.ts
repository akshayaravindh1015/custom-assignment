import { Animal } from './animal.class';

export class Bird extends Animal {
    move() {
        console.log("bird will fly");
    }

}