export class LivingCreature {
  public name: string;
  public numberOfHearts: number;

  // constructor() {}

  constructor(numOfHearts: number, name: string) {
    this.numberOfHearts = numOfHearts;
    this.name = name;
  }

  public breathe() {
    console.log("this crature is breathing");
  }

  public move() {
    console.log("creature is moving");
  }
}
