import { FlapYOurGills } from "../interfaces/flap-your-gills";
import { LivingCreature } from "./living-thing.class";

export class Animal extends LivingCreature implements FlapYOurGills {
  public numberOfLegs: number;

  constructor(name: string, numOfHearts: number, numOfLegs: number) {
    super(numOfHearts, name);
    this.numberOfLegs = numOfLegs;
  }
  wannaFuck(someValue: string) {
    return false;
  }
}
