import { Animal } from "./animal.class";

export class Person extends Animal {
  
  constructor(name:string){
    super(name, 1, 2);
  }
  
  public think() {
    this.breathe();
    console.log(this.name + " is thinking");
    this.breathe();
  }

  public move() {
    console.log(this.name + " will walk");
  }
}
