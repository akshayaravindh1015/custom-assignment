import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExampleComponent } from './example/example.component';
import { ExampleService } from './example/example.service';



@NgModule({
  declarations: [ExampleComponent],
  imports: [
    CommonModule
  ],
  exports: [ExampleComponent],
  providers: [ExampleService]
})
export class InterfacesExplModule { }
