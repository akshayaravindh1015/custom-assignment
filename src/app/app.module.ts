import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AppComponent } from "./app.component";
import { ExampleService } from "./interfaces-expl/example/example.service";
import { MaterialModule } from "./common/util/material/material.module";
import { InterfacesExplModule } from "./interfaces-expl/interfaces-expl.module";
import { GameControllerService } from "./common/services/game-controller.service";
import { CommonService } from "./common/services/common.service";
import { ComponentsCommonModule } from "./components-common/components-common.module";
import { DataBaseService } from "./common/services/data-base.service";
import { GameArenaModule } from "./game-arena/game-arena.module";
import { AppRoutingModule } from "./app-routing.module";
import { DashBoardComponent } from "./dash-board/dash-board.component";
import { StartComponent } from "./start/start.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    AppComponent, 
    DashBoardComponent, 
    StartComponent,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    FontAwesomeModule,
    InterfacesExplModule,
    ComponentsCommonModule,
    GameArenaModule,
  ],
  providers: [
    ExampleService,
    GameControllerService,
    CommonService,
    DataBaseService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
