import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { Metrics } from "../common/models/metrics.model";
import { DataBaseService } from "../common/services/data-base.service";
import { faTrash } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: "app-dash-board",
  templateUrl: "./dash-board.component.html",
  styleUrls: ["./dash-board.component.scss"],
})
export class DashBoardComponent implements OnInit, OnDestroy {
  private metricsSubscription: Subscription;
  public metrics: Metrics;
  public isGettingDataFromFireBase: boolean = true;
  faUserDelete = faTrash;
  constructor(private _dataBaseService: DataBaseService) {}

  ngOnInit(): void {
    this.metricsSubscription = this._dataBaseService
      .metricsAsObservable()
      .subscribe((metrics) => {
        this.metrics = metrics;
        if (this.metrics.playerStats.length != 0) {
          this.isGettingDataFromFireBase = false;
        }
      });
    this._dataBaseService.fetchPlayersAndStroreInDataBaseService();
    this.isGettingDataFromFireBase = true;
  }

  deleteAllThePlayer() {
    this._dataBaseService.deleteAllPlayers();
  }

  deleteThisPlayer(playerId: string) {
    this._dataBaseService.deleteThisPlayer(playerId);
  }

  ngOnDestroy(): void {
    this.metricsSubscription.unsubscribe();
  }
}
