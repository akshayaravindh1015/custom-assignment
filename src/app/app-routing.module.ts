import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MessageComponent } from './components-common/message/message.component';
import { GameArenaComponent } from './game-arena/game-arena.component';
import { DashBoardComponent } from './dash-board/dash-board.component';
import { StartComponent } from './start/start.component';
// import { GameAuthGuard } from './common/services/game-auth-guard.service';

const routes: Routes = [
  {
    path: "",
    redirectTo: "home",
    pathMatch: "full",
  },
  {
    path: "message",
    component: MessageComponent,
  },
  {
    path: "game-arena",
    component: GameArenaComponent,
    // canActivate: [GameAuthGuard]
  },
  {
    path: "home",
    component: StartComponent,
    // canActivate: [GameAuthGuard]
  },
  {
    path: "dashboard",
    component: DashBoardComponent,
    // canActivate: [GameAuthGuard]
  },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
