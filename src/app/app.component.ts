import { Component, OnInit } from "@angular/core";
import { DataBaseService } from "./common/services/data-base.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  title = "custom-assignment";

  playerOneAns: number;
  playerTwoAns: number;

  constructor(private _dataBase: DataBaseService) {}
  ngOnInit() {
  }
}
