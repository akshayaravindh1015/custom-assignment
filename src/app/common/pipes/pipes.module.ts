import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OperatorPipe } from './operator.pipe';



@NgModule({
  declarations: [
    OperatorPipe,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    OperatorPipe,
  ]
})
export class PipesModule { }
