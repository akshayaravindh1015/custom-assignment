import { Pipe, PipeTransform } from "@angular/core";
import { DataModel } from "../models/data.model";

@Pipe({
  name: "operatorSign",
})
export class OperatorPipe implements PipeTransform {
  transform(value: DataModel): unknown {
    if (value && value.inputValue) {
      // return this._commonService.getOperationType(
      //   this.gameQuestion.inputValue,
      //   true
      // );
      if (value.inputValue.includes("addition")) {
        return "+";
      } else if (value.inputValue.includes("multiplication")) {
        return "*";
      } else if (value.inputValue.includes("subtraction")) {
        return "-";
      } else {
        return "/";
      }
    }
    return "";
  }
}
