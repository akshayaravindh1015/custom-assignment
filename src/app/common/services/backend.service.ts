import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map, tap } from "rxjs/operators";
import { PlayerStat } from "../models/player-stat.model";

@Injectable({
  providedIn: "root",
})
export class BackendService {
  private url = "https://flutter-chat-3b184.firebaseio.com/players";
  constructor(private _http: HttpClient) {}

  public addPlayers(players: PlayerStat[]): Observable<any> {
    return this._http.put(`${this.url}.json`, players);
  }

  public getPlayers(): Observable<PlayerStat[]> {
    return this._http
      .get<{ [key: string]: PlayerStat }>(`${this.url}.json`)
      .pipe(
        map((resultsObj) => {
          let playersList: PlayerStat[] = [];
          for (const [key, value] of Object.entries(resultsObj)) {
            const newPlayer = new PlayerStat(`${value.name}`, `${key}`);
            newPlayer.gamesPlayed = value.gamesPlayed;
            newPlayer.won = value.won;
            newPlayer.lost = value.lost;
            newPlayer.score = value.score;
            playersList.push(newPlayer);
          }
          return playersList;
        })
      );
  }

  public updatePlayer(playerId: string, playerStat: PlayerStat) {
    return this._http.patch(`${this.url}/${playerId}.json`, playerStat);
  }

  public addAPlayer(player: PlayerStat): Observable<any> {
    return this._http.post(`${this.url}.json`, player);
  }

  public deleteThisPlayer(playerId: string) {
    return this._http.delete(`${this.url}/${playerId}.json`);
  }
  public deleteAllPlayers() {
    return this._http.delete(`${this.url}.json`);
  }
}
