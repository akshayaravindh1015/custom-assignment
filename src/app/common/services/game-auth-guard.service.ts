import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from "@angular/router";
import { Observable } from "rxjs";
import { MessageComponent } from "src/app/components-common/message/message.component";
import { StartComponent } from "src/app/start/start.component";
import { environment } from "src/environments/environment";

import { DataBaseService } from "./data-base.service";

@Injectable({
  providedIn: "root",
})
export class GameAuthGuard implements CanActivate {
  constructor(public _dataBase: DataBaseService, public router: Router) {}

  canActivate(
    activatedRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | UrlTree {
    if (activatedRoute.component == MessageComponent) {
      return true;
    } else if (activatedRoute.component == StartComponent) {
      return !this._dataBase.canILoadGame();
    } else {
      if (this._dataBase.canILoadGame()) {
        return true;
      } else {
        return this.router.parseUrl("home");
      }
    }
  }
}
