import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { BehaviorSubject, Observable, Subject } from "rxjs";
import { AnswerEvent } from "../models/answer-event.model";
import { Metrics } from "../models/metrics.model";
import { PlayerStat } from "../models/player-stat.model";
import {
  GameControlSession,
  PlayerSessionStorage,
  SessionStorage,
} from "../models/session.model";
import { BackendService } from "./backend.service";

@Injectable({
  providedIn: "root",
})
export class DataBaseService {
  private metrics: Metrics = new Metrics();
  private session: SessionStorage = new SessionStorage();
  private metricsSubject: BehaviorSubject<Metrics> = new BehaviorSubject(
    this.metrics
  );

  constructor(
    private _backendService: BackendService,
    private router: Router
  ) {}

  public canPlayerCheat(playerId: string): boolean {
    const playerIndex = this.metrics.playerStats.findIndex(
      (pl) => pl.id == playerId
    );
    if (playerIndex !== -1) {
      return this.metrics.playerStats[playerIndex].score >= 3;
    }
    return false;
  }
  public playerCheated(playerId: string) {
    const playerIndex = this.metrics.playerStats.findIndex(
      (pl) => pl.id == playerId
    );
    let newPlayerStat = this.metrics.playerStats[playerIndex];
    newPlayerStat.score -= 3;
    this.updateTheStatsOfThisPlayer(newPlayerStat.id, newPlayerStat);
  }
  public updateTheStatsOfThisPlayer(playerId: string, playerStat: PlayerStat) {
    this._backendService.updatePlayer(playerId, playerStat).subscribe(() => {
      const playerIndex = this.metrics.playerStats.findIndex(
        (pl) => pl.id == playerId
      );
      this.metrics.playerStats[playerIndex] = playerStat;
      this.metricsSubject.next(this.metrics);
    });
  }
  public addANewPlayer(plyaer: PlayerStat) {
    this._backendService.addAPlayer(plyaer).subscribe((success) => {
      console.log("Player Added Successfully");
    });
    this.metrics.playerStats.push(plyaer);
    this.metricsSubject.next(this.metrics);
  }
  public deleteThisPlayer(playerId: string) {
    this._backendService.deleteThisPlayer(playerId).subscribe(() => {
      const playerIndex = this.metrics.playerStats.findIndex(
        (pl) => pl.id == playerId
      );
      this.metrics.playerStats.splice(playerIndex, 1);
      if (this.metrics.playerStats.length === 0) {
        this.router.navigate(["home"]);
      }
    });
  }
  public deleteAllPlayers() {
    this._backendService.deleteAllPlayers().subscribe(() => {
      this.metrics = new Metrics();
      this.metricsSubject.next(this.metrics);
      this.router.navigate(["home"]);
    });
  }
  public fetchPlayersAndStroreInDataBaseService(): void {
    this._backendService.getPlayers().subscribe((playerFromFireBase) => {
      this.metrics.playerStats = playerFromFireBase;
      this.metricsSubject.next(this.metrics);
    });
  }
  public getPlayers(): PlayerStat[] {
    return this.metrics.playerStats;
  }
  // public canILoadGame(): boolean {
  //   return this.metrics.playerStats.length !== 0;
  // }
  public loadSomeInitialPlayers(playerStats: PlayerStat[]): void {
    // this.metrics.playerStats = playerStats;
    let count = 0;
    playerStats.forEach((player) => {
      this._backendService.addAPlayer(player).subscribe(
        (result) => {
          const idFromFireBase = result.name;
          player.id = idFromFireBase;
          this.metrics.playerStats.push(player);
          count++;
          if (count == playerStats.length) {
            this.router.navigate(["game-arena"]);
          }
        },
        (error) => {
          alert(error.message);
        }
      );
    });
    // this._backendService.addPlayers(playerStats).subscribe(
    //   (success) => {
    //     console.log(success);
    //     this.router.navigate(["game-arena"]);
    //   },
    //   (error) => {
    //     alert(error.message);
    //   }
    // );
  }
  public incrementGames() {
    this.metrics.numberOfGameSPlayed++;
  }

  public gameDoneForPlayer(playerAnswer: AnswerEvent, isHeCorrect: boolean) {
    let playerIndex = this.metrics.playerStats.findIndex(
      (player) => player.id == playerAnswer.playerId
    );
    if (playerIndex == -1) {
      let newPlayerStat = new PlayerStat(
        playerAnswer.playerName,
        playerAnswer.playerId
      );
      playerIndex = this.metrics.playerStats.push(newPlayerStat) - 1;
    }
    let newPlayerStat = this.metrics.playerStats[playerIndex];
    newPlayerStat.gamesPlayed++;
    if (isHeCorrect) {
      newPlayerStat.won++;
      newPlayerStat.score += 2;
    } else {
      newPlayerStat.lost++;
      newPlayerStat.score--;
    }
    this.updateTheStatsOfThisPlayer(newPlayerStat.id, newPlayerStat);
    // this.metricsSubject.next(this.metrics);
  }

  public metricsAsObservable(): Observable<Metrics> {
    return this.metricsSubject.asObservable();
  }

  public getSessionData(): SessionStorage {
    return this.session;
  }

  public saveGameControllerSessionData(data: GameControlSession) {
    this.session.gameController = data;
  }

  public savePlayerSessionData(data: PlayerSessionStorage) {
    const playerIndex = this.session.players.findIndex(
      (pl) => pl.id === data.id
    );
    if (playerIndex != -1) {
      this.session.players[playerIndex] = data;
    } else {
      this.session.players.push(data);
    }
  }
}
