import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class CommonService {
  
  private operations: string[] = [
    "addition",
    "subtraction",
    "multiplication",
    "division",
  ];

  generateRandomNumber(lessThan: number): number {
    return Math.floor(Math.random() * lessThan) + 1;
  }
  generateRandomOpertaion(): string {
    let randomIndex = (Math.floor(Math.random() * 100) + 1) % 4;
    return this.operations[randomIndex];
  }
  calculateTheActualAnswer(val1: number,val2: number,operation: string): number {
    let answer;
    switch (operation) {
      case "addition":
        answer = val1 + val2;
        break;
      case "subtraction":
        answer = val1 - val2;
        break;
      case "multiplication":
        answer = val1 * val2;
        break;
      case "division":
        answer = val1 / val2;
        break;
    }
    return answer;
  }
  getOperationType(questionString: string,needToReturnSymbols: boolean): string {
    if (questionString.includes("addition")) {
      return needToReturnSymbols ? "+" : "addition";
    } else if (questionString.includes("multiplication")) {
      return needToReturnSymbols ? "*" : "multiplication";
    } else if (questionString.includes("subtraction")) {
      return needToReturnSymbols ? "-" : "subtraction";
    } else {
      return needToReturnSymbols ? "/" : "division";
    }
  }
  
}
