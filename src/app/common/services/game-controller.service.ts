import { Injectable, EventEmitter } from "@angular/core";
import { BehaviorSubject, Observable, Subject } from "rxjs";
import { AnswerEvent } from "../models/answer-event.model";
import { DataModel } from "../models/data.model";
import { GameControlSession } from "../models/session.model";
import { CommonService } from "./common.service";
import { DataBaseService } from "./data-base.service";

@Injectable({
  providedIn: "root",
})
export class GameControllerService {
  public currentOperation: string;
  private numberGenerated: Subject<DataModel> = new Subject<DataModel>();
  private playerAnswered: Subject<AnswerEvent> = new Subject<AnswerEvent>();
  private resetGame: Subject<boolean> = new Subject<boolean>();
  public percentageOfGame: BehaviorSubject<number> = new BehaviorSubject(0);
  // public numberGenerated: EventEmitter<DataModel> = new EventEmitter<DataModel>();
  // public playerAnswered: EventEmitter<AnswerEvent> = new EventEmitter<AnswerEvent>();
  // public player1Answered: EventEmitter<number> = new EventEmitter<number>();
  // public player2Answered: EventEmitter<number> = new EventEmitter<number>();

  constructor(
    private _commonService: CommonService,
    private _dataBaseService: DataBaseService
  ) {}

  submitMyAnswer(ans: AnswerEvent) {
    this.playerAnswered.next(ans);
  }
  newQuestionIsGenerated(gameSession: GameControlSession) {
    this.numberGenerated.next(gameSession.question);
    this._dataBaseService.saveGameControllerSessionData(gameSession);
  }
  questionGeneratedAsObs(): Observable<DataModel> {
    return this.numberGenerated.asObservable();
  }
  playerAnsweredAsObs(): Observable<AnswerEvent> {
    return this.playerAnswered.asObservable();
  }

  resetTheGame() {
    // this.numberGenerated.next(null);
    this.resetGame.next(true);
  }
  resetGameAsObs(): Observable<boolean> {
    return this.resetGame.asObservable();
  }

  generateAndStoreOperation(): string {
    this.currentOperation = this._commonService.generateRandomOpertaion();
    return this.currentOperation;
  }
  decideTheWinnerAndGetMessage(
    answerEvents: AnswerEvent[],
    correctAnswer: number
  ): string {
    let finalMessage;
    this._dataBaseService.incrementGames();
    answerEvents.forEach((ans) => {
      if (ans.answer == correctAnswer) {
        finalMessage = !!finalMessage
          ? `${finalMessage}, ${ans.playerName}`
          : `${ans.playerName}`;
        this._dataBaseService.gameDoneForPlayer(ans, true);
      } else {
        this._dataBaseService.gameDoneForPlayer(ans, false);
      }
    });
    return `${finalMessage} Won`;
  }
}
