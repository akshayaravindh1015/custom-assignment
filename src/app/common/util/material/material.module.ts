import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MatToolbarModule } from "@angular/material/toolbar";
import { MatStepperModule } from "@angular/material/stepper";
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

const declarations = [
  MatToolbarModule, 
  MatStepperModule,
  MatButtonModule,
  MatIconModule,
];

@NgModule({
  imports: [CommonModule, ...declarations],
  exports: declarations,
})
export class MaterialModule {}
