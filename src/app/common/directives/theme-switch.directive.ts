import {
  Directive,
  ElementRef,
  HostListener,
  Input,
  Renderer2,
} from "@angular/core";

@Directive({
  selector: "[themeSwitch]",
})
export class ThemeSwitchDirective {
  private defaultDarkBackGroundColor: string = "#00000093";
  private defaultDarkTextColor: string = "#fff";

  private defaultLightBackGroundColor: string = "#fff";
  private defaultLightTextColor: string = "#333";

  private currentTheme: "dark" | "light" = "dark";

  @Input() switchKey = "s";

  // private nativeElement : Node;
  constructor(private el: ElementRef, private renderer: Renderer2) {
    // this.nativeElement = el.nativeElement;
  }

  @HostListener("window:keyup", ["$event"])
  keyEvent(event: KeyboardEvent) {
    if (event.key == this.switchKey) {
      this.currentTheme = this.currentTheme == "dark" ? "light" : "dark";
      this.selectColors();
    }
  }

  private selectColors() {
    switch (this.currentTheme) {
      case "dark":
        this.renderer.setStyle(
          this.el.nativeElement,
          "background-color",
          this.defaultDarkBackGroundColor
        );
        this.renderer.setStyle(
          this.el.nativeElement,
          "color",
          this.defaultDarkTextColor
        );
        break;
      case "light":
        this.renderer.setStyle(
          this.el.nativeElement,
          "background-color",
          this.defaultLightBackGroundColor
        );
        this.renderer.setStyle(
          this.el.nativeElement,
          "color",
          this.defaultLightTextColor
        );
        break;
    }
  }
}
