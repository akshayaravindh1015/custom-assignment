import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LiteModeDirective } from './lite-mode.directive';
import { ThemeSwitchDirective } from './theme-switch.directive';

@NgModule({
  declarations: [
    LiteModeDirective,
    ThemeSwitchDirective,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    LiteModeDirective,
    ThemeSwitchDirective,
  ]
})
export class DirectivesModule { }
