import { Directive, ElementRef } from "@angular/core";

@Directive({
  selector: "[liteMode]",
})
export class LiteModeDirective {
  constructor(el: ElementRef) {
    el.nativeElement.style.backgroundColor = "#fff";
    el.nativeElement.style.color = "#333";
  }
}
