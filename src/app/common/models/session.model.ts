import { AnswerEvent } from './answer-event.model';
import { DataModel } from "./data.model";

export class PlayerSessionStorage {
  id: string;
  currentAnswer: string;
  constructor(id: string, currAns: string) {
    this.id = id;
    this.currentAnswer = currAns;
  }
}

export class GameControlSession {
  question: DataModel;
  playerAnswers: AnswerEvent[];
  gameCreatedTimeStamp: Date;
  constructor(
    playerAnswers: AnswerEvent[],
    question?: DataModel,
    gameCreatedTimeStamp?: Date
  ) {
    this.playerAnswers = playerAnswers;
    this.question = question || null;
    this.gameCreatedTimeStamp = gameCreatedTimeStamp || null;
  }
}

export class SessionStorage {
  gameController: GameControlSession;
  players: PlayerSessionStorage[];
  constructor() {
    this.gameController = new GameControlSession([]);
    this.players = [];
  }
}
