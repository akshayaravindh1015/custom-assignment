export class AnswerEvent {
  answer: number;
  playerId: string;
  playerName: string;

  constructor(playerId: string, playerName: string, answer: number) {
    this.answer = answer;
    this.playerId = playerId;
    this.playerName = playerName;
  }
}
