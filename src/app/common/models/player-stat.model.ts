export class PlayerStat {
  id: string;
  name: string;
  gamesPlayed: number;
  won: number;
  lost: number;
  score: number;
  constructor(name: string,id?: string) {
    this.id = id || null;
    this.name = name;
    this.gamesPlayed = 0;
    this.won = 0;
    this.lost = 0;
    this.score = 5;
  }
}
