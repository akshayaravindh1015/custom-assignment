import { PlayerStat } from "./player-stat.model";

export class Metrics {
  numberOfGameSPlayed: number;
  playerStats: PlayerStat[];
  constructor() {
    this.numberOfGameSPlayed = 0;
    this.playerStats = [];
  }
}
