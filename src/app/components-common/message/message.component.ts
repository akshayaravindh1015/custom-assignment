import { Component, OnInit } from "@angular/core";
import { environment } from "../../../environments/environment";

@Component({
  selector: "message",
  templateUrl: "./message.component.html",
  styleUrls: ["./message.component.scss"],
})
export class MessageComponent implements OnInit {
  isOpen: boolean = true;
  isLinear: boolean = false;
  constructor() {}

  beforeObj = { player1: "PLAYER_ONE_OBJECT", player2: "PLAYER_TWO_OBJECT" };
  afterArray = ["PLAYER_ONE_OBJECT", "PLAYER_TWO_OBJECT"];
  playerId = "${palyerId}"
  arrayObj = {
    "0": {
      id: "1",
      lost: 0,
      name: "Player 1",
      score: 5,
      won: 0,
    },
    "1": {
      id: "2",
      lost: 0,
      name: "Player 2",
      score: 5,
      won: 0,
    },
    "2": {
      id: "3",
      lost: 0,
      name: "Player 3",
      score: 5,
      won: 0,
    },
  };
  ngOnInit(): void {
    this.isLinear = environment.production;
  }
}
