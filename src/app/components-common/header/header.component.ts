import { Component, HostListener, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";

import { faUserPlus } from "@fortawesome/free-solid-svg-icons";
import { PlayerStat } from "src/app/common/models/player-stat.model";
import { DataBaseService } from "src/app/common/services/data-base.service";

@Component({
  selector: "cust-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"],
})
export class HeaderComponent implements OnInit {
  faUserPlus = faUserPlus;
  isAddingPlayer: boolean = false;

  newPlayerFormGroup: FormGroup = new FormGroup({
    playerId: new FormControl("", Validators.required),
    playerName: new FormControl("", Validators.required),
  });
  constructor(private _dataBase: DataBaseService) {}

  ngOnInit(): void {}

  addANewPlayer() {
    this.isAddingPlayer = true;
  }

  submitNewPlayer() {
    if (this.newPlayerFormGroup.valid) {
      this.isAddingPlayer = false;
      this._dataBase.addANewPlayer(
        new PlayerStat(
          this.newPlayerFormGroup.controls.playerName.value,
          // this.newPlayerFormGroup.controls.playerId.value,
        )
      );
      this.newPlayerFormGroup.reset();
    } else {
      this.newPlayerFormGroup.updateValueAndValidity();
      this.newPlayerFormGroup.markAllAsTouched();
    }
  }

  cancelAddingPlayer() {
    this.isAddingPlayer = false;
    this.newPlayerFormGroup.reset();
  }

  @HostListener("keydown", ["$event"])
  escEvent(event: KeyboardEvent) {
    if (event.key == "Escape") {
      this.newPlayerFormGroup.reset();
      this.isAddingPlayer = false;
    }
  }
}
