import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeaderComponent } from './header/header.component';
import { MessageComponent } from './message/message.component';
import { MaterialModule } from '../common/util/material/material.module';
import { RouterModule } from '@angular/router';
import { DirectivesModule } from '../common/directives/directives.module';
import { CustCardComponent } from './cust-card/cust-card.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    HeaderComponent,
    MessageComponent,
    CustCardComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    DirectivesModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    HeaderComponent,
    MessageComponent,
    CustCardComponent,
  ]
})
export class ComponentsCommonModule { }
