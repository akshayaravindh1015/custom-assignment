import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { PlayerStat } from "../common/models/player-stat.model";
import { DataBaseService } from "../common/services/data-base.service";

@Component({
  selector: "game-arena",
  templateUrl: "./game-arena.component.html",
  styleUrls: ["./game-arena.component.scss"],
})
export class GameArenaComponent implements OnInit, OnDestroy {
  players: PlayerStat[];
  metricsSubscription: Subscription;
  isGettingDataFromFireBase: boolean = true;
  constructor(private _dataBase: DataBaseService) {}

  ngOnInit(): void {
    this.metricsSubscription = this._dataBase
      .metricsAsObservable()
      .subscribe((metrics) => {
        this.players = metrics.playerStats;
        if (this.players.length != 0) {
          this.isGettingDataFromFireBase = false;
        }
      });
    this._dataBase.fetchPlayersAndStroreInDataBaseService();
    this.isGettingDataFromFireBase = true;
  }

  ngOnDestroy() {
    this.metricsSubscription.unsubscribe();
  }
}
