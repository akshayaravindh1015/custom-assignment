import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { GameArenaComponent } from "./game-arena.component";
// import { PlayerOneComponent } from './player-one/player-one.component';
// import { PlayerTwoComponent } from './player-two/player-two.component';
import { GameControllerComponent } from "./game-controller/game-controller.component";
import { FormsModule } from "@angular/forms";
import { DirectivesModule } from "../common/directives/directives.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { PlayerComponent } from "./player/player.component";
import { ComponentsCommonModule } from "../components-common/components-common.module";

@NgModule({
  declarations: [
    GameArenaComponent,
    // PlayerOneComponent,
    // PlayerTwoComponent,
    GameControllerComponent,
    PlayerComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ComponentsCommonModule,
    DirectivesModule,
    PipesModule,
  ],
  exports: [GameArenaComponent],
})
export class GameArenaModule {}
