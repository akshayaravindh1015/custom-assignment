import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";
import { AnswerEvent } from "src/app/common/models/answer-event.model";
import { GameControlSession } from "src/app/common/models/session.model";
import { DataBaseService } from "src/app/common/services/data-base.service";
import { DataModel } from "../../common/models/data.model";
import { CommonService } from "../../common/services/common.service";
import { GameControllerService } from "../../common/services/game-controller.service";

@Component({
  selector: "game-controller",
  templateUrl: "./game-controller.component.html",
  styleUrls: ["./game-controller.component.scss"],
})
export class GameControllerComponent implements OnInit, OnDestroy {
  public gameQuestion: DataModel = new DataModel();

  subscriptionToPlayerAnswers: Subscription;
  subscriptionToResetPlayer: Subscription;

  playerAnswers: AnswerEvent[] = [];
  finalMessage: string;

  numberOfPlayersAnswered: number = 0;
  gameCreatedAt: Date = null;

  constructor(
    private _gameController: GameControllerService,
    private _commonService: CommonService,
    private _dataBaseService: DataBaseService
  ) {}

  ngOnInit(): void {
    this.subscriptionToPlayerAnswers = this._gameController
      .playerAnsweredAsObs()
      .subscribe((playerAns) => {
        if (!!playerAns) {
          let playerIndex = this.playerAnswers.findIndex(
            (pl) => pl.playerId === playerAns.playerId
          );
          if (playerIndex === -1) {
            this.numberOfPlayersAnswered++;
            this._gameController.percentageOfGame.next(
              (this.numberOfPlayersAnswered * 100) /
                this._dataBaseService.getPlayers().length
            );
            this.playerAnswers.push(playerAns);
          } else {
            this.playerAnswers[playerIndex] = playerAns;
          }
        }
      });
    this.subscriptionToResetPlayer = this._gameController
      .resetGameAsObs()
      .subscribe((needToReset) => {
        if (needToReset) {
          this.playerAnswers = [];
          // this.finalMessage = null;
          this.gameQuestion = new DataModel();
          this.numberOfPlayersAnswered = 0;
          this._gameController.percentageOfGame.next(0);
          this.gameCreatedAt = null;
          this._dataBaseService.saveGameControllerSessionData(
            new GameControlSession(
              this.playerAnswers,
              this.gameQuestion,
              this.gameCreatedAt,
            )
          );
        }
      });
    let session = this._dataBaseService.getSessionData();
    if (!!session.gameController.question) {
      this.gameQuestion = session.gameController.question;
      this.gameCreatedAt = session.gameController.gameCreatedTimeStamp;
    }
    this.playerAnswers = session.gameController.playerAnswers;
    this.numberOfPlayersAnswered = session.gameController.playerAnswers.length;
    this._gameController.percentageOfGame.next(
      (this.numberOfPlayersAnswered * 100) /
        this._dataBaseService.getPlayers().length
    );
  }

  startANewGame(fromWinnerAnnouncement?: boolean): void {
    if (fromWinnerAnnouncement) {
      this.finalMessage = null;
    }
    this.gameCreatedAt = new Date();
    this.gameQuestion.generatedValue1 = this._commonService.generateRandomNumber(
      100
    );
    this.gameQuestion.generatedValue2 = this._commonService.generateRandomNumber(
      10
    );
    this.gameQuestion.inputValue = `What is the ${this._gameController.generateAndStoreOperation()} of ${
      this.gameQuestion.generatedValue1
    } and ${this.gameQuestion.generatedValue2}`;
    this._gameController.newQuestionIsGenerated(
      new GameControlSession(
        this.playerAnswers,
        this.gameQuestion,
        this.gameCreatedAt
      )
    );
  }

  resetTheGame() {
    this._gameController.resetTheGame();
  }

  decideTheWinner(): void {
    const answer = this._commonService.calculateTheActualAnswer(
      this.gameQuestion.generatedValue1,
      this.gameQuestion.generatedValue2,
      this._gameController.currentOperation
    );
    this.finalMessage = this._gameController.decideTheWinnerAndGetMessage(
      this.playerAnswers,
      answer
    );
    this.resetTheGame();
  }

  get gameControllerTextFontSize(): string {
    return `${15 + 10 * this.numberOfPlayersAnswered}px`;
  }
  ngOnDestroy() {
    this.subscriptionToPlayerAnswers.unsubscribe();
    this.subscriptionToResetPlayer.unsubscribe();
    this._dataBaseService.saveGameControllerSessionData(
      new GameControlSession(
        this.playerAnswers,
        this.gameQuestion,
        this.gameCreatedAt
      )
    );
  }
}
