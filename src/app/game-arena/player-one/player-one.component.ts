import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  OnDestroy,
} from "@angular/core";
import { Subscription } from "rxjs";
import { PlayerSessionStorage } from "src/app/common/models/session.model";
import { DataBaseService } from "src/app/common/services/data-base.service";
import { AnswerEvent } from "../../common/models/answer-event.model";
import { DataModel } from "../../common/models/data.model";
import { CommonService } from "../../common/services/common.service";
import { GameControllerService } from "../../common/services/game-controller.service";

@Component({
  selector: "player-one",
  templateUrl: "./player-one.component.html",
  styleUrls: ["./player-one.component.scss"],
})
export class PlayerOneComponent implements OnInit, OnDestroy {
  // @Input() gameQuestion: DataModel;
  gameQuestion: DataModel;
  // @Output() questionIsAnswered: EventEmitter<number> = new EventEmitter<number>();
  answerValue: string;
  subscriptionToGameQuestion: Subscription;
  subscriptionToResetPlayer: Subscription;
  constructor(
    private _gameController: GameControllerService,
    private _commonService: CommonService,
    private _dataBaseService: DataBaseService
  ) {}

  ngOnInit(): void {
    this.subscriptionToGameQuestion = this._gameController
      .questionGeneratedAsObs()
      .subscribe((data) => (this.gameQuestion = data));
    this.subscriptionToResetPlayer = this._gameController
      .resetGameAsObs()
      .subscribe((needToReset) => {
        if (needToReset) {
          this.gameQuestion = null;
          this.answerValue = "";
          this._dataBaseService.savePlayerSessionData(
            new PlayerSessionStorage(),
            "player1"
          );
        }
      });
    let session = this._dataBaseService.getSessionData();
    if (!!session.players.player1.currentAnswer) {
      this.answerValue = session.players.player1.currentAnswer;
    }
    if (
      !!session.gameController.question &&
      Object.keys(session.gameController.question).length != 0
    ) {
      this.gameQuestion = session.gameController.question;
    }
  }

  sendAnswer() {
    // this.questionIsAnswered.emit(+this.answerValue);
    // this._gameController.player1Answered.emit(+this.answerValue);
    let playerOneAnswer = new AnswerEvent();
    playerOneAnswer.answer = +this.answerValue;
    playerOneAnswer.playerType = "playerOne";
    // this._gameController.playerAnswered.emit(playerOneAnswer);
    this._gameController.submitMyAnswer(playerOneAnswer);
  }

  cheatTheGame() {
    const answer = this._commonService.calculateTheActualAnswer(
      this.gameQuestion.generatedValue1,
      this.gameQuestion.generatedValue2,
      // this._commonService.getOperationType(this.gameQuestion.inputValue, false)
      this._gameController.currentOperation
    );
    this.answerValue = `${answer}`;
  }

  checkMyLuck() {
    this.answerValue = `${this._commonService.generateRandomNumber(100)}`;
  }

  get operationType(): string {
    if (!!this.gameQuestion && !!this.gameQuestion.inputValue) {
      return this._commonService.getOperationType(
        this.gameQuestion.inputValue,
        true
      );
    }
    return "";
  }

  ngOnDestroy() {
    this.subscriptionToGameQuestion.unsubscribe();
    this.subscriptionToResetPlayer.unsubscribe();
    this._dataBaseService.savePlayerSessionData(
      new PlayerSessionStorage(this.answerValue),
      "player1"
    );
  }
}
