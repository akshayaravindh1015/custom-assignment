import { Component, OnInit, OnDestroy, Input } from "@angular/core";
import { Subscription } from "rxjs";
import { PlayerStat } from "src/app/common/models/player-stat.model";
import { PlayerSessionStorage } from "src/app/common/models/session.model";
import { DataBaseService } from "src/app/common/services/data-base.service";
import { AnswerEvent } from "../../common/models/answer-event.model";
import { DataModel } from "../../common/models/data.model";
import { CommonService } from "../../common/services/common.service";
import { GameControllerService } from "../../common/services/game-controller.service";

@Component({
  selector: "player",
  templateUrl: "./player.component.html",
  styleUrls: ["./player.component.scss"],
})
export class PlayerComponent implements OnInit, OnDestroy {
  @Input() id: string;

  playerDetails: PlayerStat;

  public gameQuestion: DataModel;
  public answerValue: string;

  private subscriptoinToMetrics: Subscription;
  private subscriptionToGameQuestion: Subscription;
  private subscriptionToResetPlayer: Subscription;

  public progressPercentage: number;

  constructor(
    private _gameController: GameControllerService,
    private _commonService: CommonService,
    private _dataBaseService: DataBaseService
  ) {}

  ngOnInit(): void {
    this.initalizeAllSubscriptions();
    this.retainSession();
    this._gameController.percentageOfGame.subscribe(
      (val) => (this.progressPercentage = val)
    );
  }

  /** Initialization Functions */
  private retainSession(): void {
    let session = this._dataBaseService.getSessionData();
    const playerIndex = session.players.findIndex(
      (pl) => pl.id === this.playerDetails.id
    );
    if (playerIndex !== -1) {
      this.answerValue = session.players[playerIndex].currentAnswer;
    }
    if (
      !!session.gameController.question &&
      Object.keys(session.gameController.question).length != 0
    ) {
      this.gameQuestion = session.gameController.question;
    }
  }
  private initalizeAllSubscriptions(): void {
    this.subscriptoinToMetrics = this._dataBaseService
      .metricsAsObservable()
      .subscribe((metrics) => {
        const playerIndex = metrics.playerStats.findIndex(
          (pl) => pl.id == this.id
        );
        this.playerDetails = metrics.playerStats[playerIndex];
      });
    this.subscriptionToGameQuestion = this._gameController
      .questionGeneratedAsObs()
      .subscribe((data) => (this.gameQuestion = data));
    this.subscriptionToResetPlayer = this._gameController
      .resetGameAsObs()
      .subscribe((needToReset) => {
        if (needToReset) {
          this.gameQuestion = null;
          this.answerValue = "";
          this._dataBaseService.savePlayerSessionData(
            new PlayerSessionStorage(
              this.playerDetails.id,
              this.playerDetails.name
            )
          );
        }
      });
  }

  /** Button click & Other handlers */
  public sendAnswer(): void {
    let playerOneAnswer = new AnswerEvent(
      this.playerDetails.id,
      this.playerDetails.name,
      +this.answerValue
    );
    this._gameController.submitMyAnswer(playerOneAnswer);
  }
  public cheatTheGame(): void {
    if (this._dataBaseService.canPlayerCheat(this.playerDetails.id)) {
      const answer = this._commonService.calculateTheActualAnswer(
        this.gameQuestion.generatedValue1,
        this.gameQuestion.generatedValue2,
        this._gameController.currentOperation
      );
      this.answerValue = `${answer}`;
      this._dataBaseService.playerCheated(this.playerDetails.id);
    }
  }
  public checkMyLuck(): void {
    this.answerValue = `${this._commonService.generateRandomNumber(100)}`;
  }

  /** Getters and Setters */
  get operationType(): string {
    if (!!this.gameQuestion && !!this.gameQuestion.inputValue) {
      return this._commonService.getOperationType(
        this.gameQuestion.inputValue,
        true
      );
    }
    return "";
  }

  /** End functions */
  ngOnDestroy() {
    this.subscriptionToGameQuestion.unsubscribe();
    this.subscriptionToResetPlayer.unsubscribe();
    this.subscriptoinToMetrics.unsubscribe();
    this._dataBaseService.savePlayerSessionData(
      new PlayerSessionStorage(this.playerDetails.id, this.answerValue)
    );
  }
}
