import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  OnDestroy,
} from "@angular/core";
import { Subscription } from "rxjs";
import { PlayerSessionStorage } from "src/app/common/models/session.model";
import { DataBaseService } from "src/app/common/services/data-base.service";
import { AnswerEvent } from "../../common/models/answer-event.model";
import { DataModel } from "../../common/models/data.model";
import { CommonService } from "../../common/services/common.service";
import { GameControllerService } from "../../common/services/game-controller.service";

@Component({
  selector: "player-two",
  templateUrl: "./player-two.component.html",
  styleUrls: [
    "./player-two.component.scss",
    "../player-one/player-one.component.scss",
  ],
})
export class PlayerTwoComponent implements OnInit, OnDestroy {
  // @Input() gameQuestion: DataModel;
  gameQuestion: DataModel;
  // @Output() questionIsAnswered: EventEmitter<number> = new EventEmitter<number>();
  @ViewChild("answer") answer: ElementRef;
  subscriptionToGameQuestion: Subscription;
  subscriptionToResetPlayer: Subscription;
  constructor(
    private _gameController: GameControllerService,
    private _commonService: CommonService,
    private _dataBaseService: DataBaseService
  ) {}

  ngOnInit(): void {
    this.subscriptionToGameQuestion = this._gameController
      .questionGeneratedAsObs()
      .subscribe((data) => (this.gameQuestion = data));
    this.subscriptionToResetPlayer = this._gameController
      .resetGameAsObs()
      .subscribe((needToReset) => {
        if (needToReset) {
          this.gameQuestion = null;
          this.answer.nativeElement.value = "";
          this._dataBaseService.savePlayerSessionData(
            new PlayerSessionStorage(),
            "player2"
          );
        }
      });
    let session = this._dataBaseService.getSessionData();
    if (
      !!session.gameController.question &&
      Object.keys(session.gameController.question).length != 0
    ) {
      this.gameQuestion = session.gameController.question;
    }
    if (!!session.players.player2.currentAnswer) {
      setTimeout(() => {
        this.answer.nativeElement.value = session.players.player2.currentAnswer;
      }, 0);
    }
  }

  sendAnswer() {
    // this.questionIsAnswered.emit(+this.answer.nativeElement.value);
    // this._gameController.player2Answered.emit(+this.answer.nativeElement.value);
    let playerTwoAnswer = new AnswerEvent(
      +this.answer.nativeElement.value,
      "playerTwo"
    );
    // this._gameController.playerAnswered.emit(playerTwoAnswer);
    this._gameController.submitMyAnswer(playerTwoAnswer);
  }

  cheatTheGame() {
    const answer = this._commonService.calculateTheActualAnswer(
      this.gameQuestion.generatedValue1,
      this.gameQuestion.generatedValue2,
      // this._commonService.getOperationType(this.gameQuestion.inputValue, false)
      this._gameController.currentOperation
    );
    (<HTMLInputElement>this.answer.nativeElement).value = `${answer}`;
  }
  checkMyLuck() {
    (<HTMLInputElement>(
      this.answer.nativeElement
    )).value = `${this._commonService.generateRandomNumber(100)}`;
  }

  get operationType(): string {
    if (!!this.gameQuestion && !!this.gameQuestion.inputValue) {
      return this._commonService.getOperationType(
        this.gameQuestion.inputValue,
        false
      );
    }
    return "";
  }

  ngOnDestroy() {
    this.subscriptionToGameQuestion.unsubscribe();
    this.subscriptionToResetPlayer.unsubscribe();
    if (!!this.answer) {
      this._dataBaseService.savePlayerSessionData(
        new PlayerSessionStorage(this.answer.nativeElement.value),
        "player2"
      );
    }
  }
}
