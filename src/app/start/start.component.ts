import { Component, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormArray,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { Router } from "@angular/router";
import { PlayerStat } from "../common/models/player-stat.model";
import { DataBaseService } from "../common/services/data-base.service";
// import { FormControl } from "@angular/forms";

@Component({
  selector: "app-start",
  templateUrl: "./start.component.html",
  styleUrls: ["./start.component.scss"],
})
export class StartComponent implements OnInit {
  numberOfPlayers = new FormControl("");

  playersArray = new FormArray([]);
  constructor(private router: Router, private _dataBase: DataBaseService) {}

  counter: number = 0;
  ngOnInit(): void {
    this.numberOfPlayers.valueChanges.subscribe((value) => {
      if (value < this.playersArray.length) {
        const diff = this.playersArray.length - value;
        for (let i = 0; i < diff; i++) {
          this.playersArray.controls.pop();
          this.counter--;
        }
      } else if (value > this.playersArray.length) {
        const diff = value - this.playersArray.length;
        for (let i = 0; i < diff; i++) {
          this.playersArray.controls.push(
            new FormGroup({
              // playerId: new FormControl(`${++this.counter}`, [
              //   Validators.required,
              //   this.duplicateIdValidator.bind(this),
              // ]),
              playerName: new FormControl(`Player ${++this.counter}`, [Validators.required]),
            })
          );
        }
      }
    });
  }

  submit() {
    let isFormValid: boolean = true;
    let playerStats: PlayerStat[] = [];
    if (!this.numberOfPlayers.value) {
      alert("Select atleast 2 number of players");
      return;
    }
    this.playersArray.controls.every((formGroup) => {
      if (formGroup.valid) {
        let playerGroup = <FormGroup>formGroup;
        playerStats.push(
          new PlayerStat(
            playerGroup.controls.playerName.value
            // playerGroup.controls.playerId.value,
          )
        );
        return true;
      } else {
        alert("Enter valid values");
        isFormValid = false;
        return false;
      }
    });
    if (isFormValid) {
      this._dataBase.loadSomeInitialPlayers(playerStats);
      // this.router.navigate(["game-arena"]);
    }
  }

  // duplicateIdValidator(control: AbstractControl): { [s: string]: boolean } {
  //   let errorObj: { [s: string]: boolean } = null;
  //   this.playersArray.controls.forEach((formGroup) => {
  //     let playerGroup = <FormGroup>formGroup;
  //     if (
  //       playerGroup.controls.playerId !== control &&
  //       playerGroup.controls.playerId.value == control.value
  //     ) {
  //       errorObj = { duplicateId: true };
  //     }
  //   });
  //   return errorObj;
  // }
}
